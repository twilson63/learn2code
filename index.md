## Learn to Code

An absolute beginners workshop to learning to code.

> This site is a notes site and is not meant to be consumed alone, it 
is the notes for the instructor led course.

## Introduction

[Start](introduction)

## Guess a Number

> Enter the whirlwind - dive deep into code

[guess a number project](guess-a-number)

## AdLibs

> Focus more on HTML and CSS

[adlib project](adlib)

## Mazes

> Focus on control flow

## Magic Eight Ball

> Learn Arrays and animations and css 

## Game of Life

> Learn separating business and presentation logic 

## Todo Application

> The standard of a crud application




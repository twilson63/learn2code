## Approach to learning

Using a first principals approach to learning is the process of breaking
things down to the smallest working parts, then building them back up yourself. This process removes the magic from the tool or technology and gives you a true understanding of what is going on as you use it.

It gives you access to good solid questions to ask as you try out a new library on tool. It also may lead to exposing some risks or side effects early on so that you don't commit to something too early.

At the end of the day you will understand things with clarity and start to move and thrive with the creative process.

# Programming Basics with Javascript

## Introduction

In order to program the computer you need to learn a language that bridges your intentions into computer actions. Computers only understand 1s and 0s and each processing unit contains billions of bits that can be set in a state of 1 or a state of 0. You could instruct the computer by providing sets of 1s and 0s but that would be very difficult given the fact that when you look at a screen of 1s and 0s it does not make a lot of sense to you. So we need a way to bridge the human word of language and symbols to the computer world of 1s and 0s. This is where programming langugages, methodologies and practices come in to scope.

Javascript is a jit language which means that it compiles and translates its code into runnable machine code (1s and 0s) in real time.

So we are going to learn programming basics in Javascript. The great thing about Javascript is that it is already own your browser. You can open any modern browser and go to the developer tools javascript console and play around with Javascript.

## Types and Operators

To get started understanding programming languages you need to learn the data type primatives and some common operators. 

What is data?

Facts and stats collected together for reference or analysis

What is a Type?

a data type or type is an attribute of data which tells the compiler or interpreter how the programmer intends to use the data.

Javascript has the following types

* undefined
* null
* Boolean (true or false)
* Number 
* String (collection of alphanumeric characters)
* Function (a named section that performs a specific task)
* Array (unordered, unspecified collection of data)
* Object (a labeled collection of data)

What is an operator?

an operator is a symbol that is capable of manipulating a value or set of values.

Examples

`1 + 2` // 3
`1 === 1` // true
`1 !== 1` // false
`2 * 2` // 4

Common Operators (Math and Logical)

`+ - * / === !== < > <= >= ! || &&`

## Expressions/Statements

An expression is a `value operator value` grouping that can be evaluated by the computer.

When creating a line of code or group of code like `[value] [operator] [value]` you are creating an expression.

## Variables/Assignment

a item that may take on more than one value during the runtime of a program

Keywords

var, let, const

Examples

`var guessesCount = 1`

`var guesses = 'Previous guesses: 50 25 10'`

`var gameOver = false`

Ok, you may notice something here, we are using the key word to create the name of our variable, but we are using an `=` symbol then we are writing a value. This is called `assignment`, it is where we assign a value to the variable. You can think of taking a value and slapping a label on it.

Variables can be assigned to different values at different times.

```
// initialize variable
var guessCount = 1

// later in the program
guessCount = 2

// later in the program
guessCount = 3
```

In this example we are re-assigning the variable `guessCount` to 2, then 3. We only use the `var` keyword to initialize the variable.

Being able to set values to variables allows the flexibility in programs to pass around values within the instructions without having to re specify the value each time. This makes it easier to maintain programs.

For example, if I have three statements that use the value 2.

```
var add2and2 = 2 + 2
var add2and4 = 2 + 4
var add2and8 = 2 + 8
```

If I wanted to change the value 2 with 3, I will need to go to each line and change 2 to 3. While in three lines, this is not a big deal, but what about an application with 100s of lines or 1000s of lines.

Using a `var` for the value 2.

```
var x = 2
var addxand2 = x + 2
var addxand4 = x + 4
var addxand8 = x + 8
```

Now if I want to change 2 to 3, I can change on line and the program will accept the new value throughout the rest of program.

## Conditionals

Assignments and operators is a good start, but applications take input and return output, and the application will need to make decisions based on the input and other state. The ability to ask questions and then go to one path or another path based on the answer is called conditionals. 

> What is state? State is the current representation of all of the available values at a given point in time.

For example, if the user guessed 50 and the random number was 60, we would want to let the user know that their guess was lower than the random number.

```
// userGuess = 50 and randomNumber = 60
if (userGuess < randomNumber) {
  hiOrLow = 'Guess is lower than answer!'
} else {
  hiOrLow = 'Guess is higher than answer!'
}
```

In this example we are using an `if else` statement to ask if the value in the `userGuess` variable is less than the `randomNumber` variable, if true, then assign the hiOrLow variable to the following string of characters, 'Guess is lower than answer!'. if false then assign the hiOrLow variable to the following string of characters, 'Guess is higher than answer!'. An `if` statement is the common way to perform conditional checking in your application, it is very powerful and gives you the ability to instruct the application to have different output based on the input.



## Arrays

## Loops

## Functions



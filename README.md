# Learn to Code

A workshop for people who want to know how coding works or just getting started to code. This work should is broken into 6 one hour lessons, each lesson is composed of 30 minutes of lecture/demo and 30 minutes of practice. ** If you want to retain this material you will need to practice much more than just in the lesson.

## Lessons

* 1. Introduction
* 2. HTML and CSS - (MadLibs)
* 3. Coding Basics - (Mazes) (Chesters Challenge)
* 4. Magic Eight Ball
* 5. Game of Life
* 6. Todo App 

Bonus: Snake Game

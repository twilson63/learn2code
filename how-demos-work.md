### How demos work

This is an imersive workshop

> What does imersive workshop mean?

An imersive workshop is where you learn by doing as much as possible.

No lecture will last more than 30 minutes
Focus on the pratical aspect as much as possible
Empower/Teach solid learning skills

The process

* Watch - watch the instructor do the project
* Pair - work through the project with the instructor
* Solo - work though the project on your own

* Repeat - watch, pair, solo

This three step flow, gives you a great pattern to learn, and create retention, you need to allow your brain to asorb the instruction and process the information, then you need to build muscle memory on how to use the information, finally you need to test how well you really comprehend the concept by doing it yourself. 

Now you won't consume all of the information on the first pass you will want to repeat this pattern several times until it starts to feel natural or familiar. So we will record each demo and you can practice this process on your own time.

It is also important to know, we will not dive deep into the why and provide deep background on the science behind the technology. The main reason for this, is that you can look up the why yourselves, google is great at that. Here are some resources that you may want to use as trusted resources:

* Mozilla Developer Network - https://developer.mozilla.org
* Wikipedia - https://wikipedia.org

There is a lot of information on the internet and it can quickly get old and out dated. Until you get a feel for what sources you can trust, I recommend to stick to these resources.

> Skeptics, it is ok to doubt this process, everyone is wired to learn differently, some people need to know every bit of how it works behind the scenes before they can move forward, others learn visually, by visualizing how things work, etc. But this process works for a large number of people and as you start to build a mental model of how everything is working you can step back and go deep in a particular area.




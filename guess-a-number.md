## Guess a Number

Learn2Code workshop 1

https://glitch.com/edit/#!/learn2code-guess-a-number


In this workshop, we will demo a fully working frontend application. There will be plenty of concepts that you don't understand, possibly all the things we are doing in this project. The purpose of this project is not to understand everything, but to get used to the coding and environment, and see an end to end working example, how it is built and get a grasp of how HTML, CSS, and JavaScript work together to create an application. 

> Warning, you will get frustrated, you will try to add syntax and it will not compile, it will give you errors you don't understand, and the process will seem very awkward. This is ok, it does not mean you are not capable of understanding how to code, everyone started from the same place you did. It takes time, some people may seem to pick things up faster than others, but if you are determined and stick with it, it will come together.


[Coding Environment Setup](code-env)

### Requirements

Create a guess a number game. When the game starts the application
picks a number from 1 - 10. Then as a user I want the opportunity to
input a number from 1 to 10 and press the `guess` button. The application
should tell me if I was too low, high or right. If I did not get the
answer then I want to be able to continue to try. Once I do get
the answer, I want to have the option to start a new game. Lastly, I
would like to see metrics on how many guesses it took to get to the
right answer.

### Background

The purpose of this project is to see a small application that show cases the act of coding for the browser. By leveraging HTML, CSS, JS to build a simple application.

### Steps

* Enhance logging to view insights
* Create a function to randomly pick a number between 1 and 10
* Create a variable to store the answer 
* Create function to guess between 1 and 10
* Create a function to reset the answer ie NewGame
* Create the game play form
* Create an Introduction view
* Add the ability to hide and show each view
* Create stats for each game

#### Enhance logging to view insights

``` html
<script>
var log = ''
var consolelog = console.log

console.log = function (txt) {
  log += '\n' + txt
  consolelog(txt)
}

</script>

{#if log}
<pre class="log">{log}</pre>
{/if}
<style>
  .log {
    border: 2px solid rgba(0,0,0,.2);
    border-radius: 4px;
    padding: 8px;
    margin: 4px;
  }
</style>
```

#### Create a function to picks a number from 1 - 10

``` js
function pickNumber() {
  return Math.floor(Math.random() * Math.floor(10)) + 1
}
```

#### Create a variable to store the number in

``` js
var answer 

answer = pickNumber()
console.log('answer: ' + answer)
```

#### Create a function to guess the number

``` js
function guessNumber(n) {
  if (n < answer) {
    return n + ' is lower than the number'
  } else if (n > answer) {
    return n + ' is greater than the number'
  } else if (n === answer) {
    return n + ' is the correct answer'
  }
}
```

### Create a game form view

form

```
div class="game-form">
  <div>
    your number:
  </div>
  <input type="number" bind:value="{number}" />
  <div class="button-group">
    <button on:click="{doGuess}">
      Guess
    </button>
  </div>
  <div>
    {result}
  </div>
</div>
```

add variables

``` js
var answer, number, result
```


doGuess function

``` js
function doGuess() {
  result = guessNumber(number)
  number = null
}
 
// set answer
answer = pickNumber()

```

style form

``` css
.game-form {
  border: 2px solid #82BC00;
  border-radius: 1px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  padding: 24px;
}
.label {
  margin: 8px 0;
  
}
button {
  border: 1px solid #82BC00;
  border-radus: 8px;
  background-color: #82BC00;
  color: white;
  
}
.result {
  border: 1px solid black;
  border-radius: 8px;
  margin: 8px 0;
  padding: 24px;
  width: 100%;
}

```

### Create new game function and button

TODO:

---

Lets break this down into parts.

* Logic

  - variables

variables are buckets or placeholders in memory that can hold information or `state` during the execution of the program

  - global scope

scope is the range in the application that memory is visible, we can restrict scope in three ways, global, local, and lexical. Global is simple, when defined, it means the whole app can see the variable and access its 
value.

  - functions

functions are a hard feature to grasp, the abstraction is really unique to programming. Basically, a function names a set of instructions that can be invoked within the defined scope of the application. When invoked it will take some input, do some processing and return some output.

  - operators (assignment, comparison)

operators are symbols in the programming language that perform primitive
functionality. 

assignment uses the equals symbol to assign a value to a variable.
comparison uses the less than and greater than symbols to return a true or false based on the comparison of two values.
there are more operators, but we can dig more into those as we build new stuff.

  - control logic - (if statement)

control logic can change the flow of logic in a program, if this statement is true do this thing, if not do this other thing, is a common example of control logic. This is called and if then statement. There are other control flow commands we will learn in future lessons.

* Content 

  - elements (div, input, button, pre)

elements are declarative blocks that surround content, letting the browser know how to manage that content.

A `div` element is an element that has no formating except that the display is a block, which means the width will always be 100% of the parent and the content will start on a new row and additional content will start on a new row.

  - attributes (class)

Inner name value pairs separated by an equals sign are know as attributes, these attributes can help connect style and functionality to content.

In this example we are using the class attribute, this attribute helps map style definitions to our content.

* Style

  - selectors ("dot")

selectors are the keys followed by a left bracket and right bracket, within the brackets are properities and values. Each selector can either be a html element, which will indicate, adjust these properties for all the elements that are `div` for example. When using a `dot` as a prefix this maps the selector to a class name, for all elements using that same class name, apply values to the specified properties.

  - properties (font-size, border, border-radius, margin, padding, width)

properities are the css features of elements in html, their is a large list of properties but you will use a lot of the core properties over and 
over for your elements. 

  - values (px, em, etc)

values represent the values for a given property these can be represented is several units, for example, a string, or pixels (px), or em (font-size of element), or rem(font-size of root). There are a lot more values to learn, but the point is that you can set values to properties using selectors to specify specific elements.

### Weird syntax

You may notice some weird syntax in the markup:

```
<div>
  {result}
</div>
```

```
<button on:click="{doGuess}">Guess</button>
```

```
<input bind:value="{number}" />
```

We are using a framework to help us with some boilier plate functionality, this framework uses `curly` braces to interact/create placeholders or binding glue between the state in code to the presentation.

Here is a simple example:

```
<script>
let name = 'world'
</script>
<input bind:value={name} />
<div>Hello {name}</div>
```

https://glitch.com/edit/#!/learn2code-example-1


### Summary

In this lesson, we jumped right into coding and there are a lot of things that don't make sense, thats ok, it takes time to get familiar with the concepts and understand things. Continue to practice this project, capture questions that you don't understand. Etc. At first the syntax looks confusing and it is a real barrier, but as you work with it more and get results you will adapt to it.

Don't give up! You don't learn to code overnight, there are so many concepts you need to understand and you will get there, it just takes persistence and time.



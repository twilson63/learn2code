## Introduction to Coding

### What is coding?

The processing of creating instructions for how a machine should take in input, process and return output. 

> Why a machine?

> A computer is a programmable machine that can take dynamic instructions
to accomplish differnt tasks.

In learning to code, we are going to learn how to build applications that use the web browser to take input, process and return output. Web Browsers are the most common way to deliver applications to users all over the world.

### What is a program?

A program is how we define the set of instructions that execute on the machine to provide an usable application to the user. The machine provides memory and processing power to run the program.

### What are languages?

Languages are the structures programmers use to create instructions that machines can interpet and run. The three languages we will be learning in this workshop are:

* HTML
* CSS
* JavaScript

### HTML

HTML stands for HyperText Markup Language, and is the declarative language of a web browser. This instructs the browser what to show in the viewport.

### CSS

CSS stands for cascading stylesheets and this language also exists for the browser, but it focuses on how to style the content in the browser, think, fonts, colors, shapes, animation.

### JavaScript

Javascript is a programming language that can run live in the browers to process input and create output in terms of html. Some call it the glue of the internet. Every browser has a built in JS compilier and a browser is available for every computer device.

### Web Stack 

Learning these three languages gives you the building blocks of creating web applications, but also give you tools to create desktop and mobile applications too.

### Demo

In this demo, we will use HTML, CSS and JavaScript to create a simple guess a number application, this application will not only give you a brief tour of these technologies, but also show how they work together to create an application.

* html: content
* css: style
* javascript: workflow and processing

### How demos work

This is an imersive workshop

> What does imersive workshop mean?

An imersive workshop is where you learn by doing as much as possible, we will not get bogged down in boring lectures that are not retained, it is very much a hands on. And this is how it will work. 

* Watch me
* Do it together
* Do it yourself

* Repeat

This three step flow, gives you a great pattern to learn, and create retention, you need to allow your brain to asorb the instruction and process the information, then you need to build muscle memory on how to use the information, finally you need to test how well you really comprehend the concept by doing it yourself. Now you want consume all of the information on the first pass you will want to repeat this pattern several times until it starts to feel natural or familiar. So we will record each demo and you can practice this process on your own time.

It is also important to know, we will not dive deep into the why and provide deep background on the science behind the technology. The main reason for this, is that you can look up the why yourselves, google is great at that. Here are some resources that you may want to use as trusted resources:

* Mozilla Developer Network - https://developer.mozilla.org
* Wikipedia - https://wikipedia.org

There is a lot of information on the internet and it can quickly get old and out dated. Until you get a feel for what sources you can trust, I recommend to stick to these resources.

 

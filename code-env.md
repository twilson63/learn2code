### Coding Environment

We will be using an online environment called `Glitch`, this environment, will allow you to share your code with the instructor, so that help can happen in an immersive way.

### Create an free account

If you don't have an account already, go ahead and create a free glitch 
account.

```
https://glitch.com/signin
```

You can use the e-mail magic link, if you don't have google, github or facebook account.

This account will set you up with a free workspace to save all of your products and it gives you the powerful developer tools all ready to go, no need to install anything on your computer. 

### New Project

Once you are registered with `glitch`, click on the new project button.

Select `clone from github` and type `https://github.com/twilson63/webapp.git`

This will pull the project template that we will be using in the course.

Once it is installed, you should see a file panel on the left side of the screen and on the right you should see a code panel showing the readme file.




